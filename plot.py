import matplotlib.pyplot as plt
import re

def create_phys_array(arr):
    '''
    Parameter:
        arr(array): array with the data for pt, eta, phi for all events of one particle
    Returns:
        filtered_arr(array): filtered array where all the columns with zero pt were removed
    
    '''
    
    # Create a boolean mask where the first row is not zero
    mask = arr[0] != 0

    # Use the mask to select columns
    filtered_arr = arr[:, mask]
    
    return(filtered_arr)


def sanitize_label_for_filename(label):
    """
    Sanitize a label to be used in a filename by removing special characters
    and LaTeX commands.
    """
    # Remove LaTeX commands
    label = re.sub(r'\\[^\s]+', '', label)
    # Replace special characters with underscores
    label = re.sub(r'[^\w\s-]', '', label)
    # Replace whitespace with underscores
    label = re.sub(r'\s+', '_', label)
    return label


def plot_input(bkg, sig, title, process, additional_params=[]):
    '''
    Parameters:
        bkg(array): array of background particle
        sig(array): array of signal particle
        additional_params(list): list of additional parameter names to plot
        title(str): title for the plot
        process (str): name of signal process (might contain latex)
    Returns:
        Plot: Plot of pt, eta, phi, and additional parameters of the first (leading) particle of bkg and sig
    '''
    
    # Filter the arrays for physical output only, i.e., no pt = 0
    filtered_bkg = create_phys_array(bkg)
    filtered_sig = create_phys_array(sig)
    
    # Number of default parameters (pt, eta, phi)
    num_default_params = 3
    all_params = ['pt', 'eta', 'phi'] + additional_params
    
    # Initialize the dictionary to store bkg and sig data for each parameter
    bkg_data = {'pt': filtered_bkg[0,:], 'eta': filtered_bkg[1,:], 'phi': filtered_bkg[2,:]}
    sig_data = {'pt': filtered_sig[0,:], 'eta': filtered_sig[1,:], 'phi': filtered_sig[2,:]}
    
    # Add additional parameters to the dictionary
    for idx, param in enumerate(additional_params):
        bkg_data[param] = filtered_bkg[num_default_params + idx, :]
        sig_data[param] = filtered_sig[num_default_params + idx, :]
        
    # Sanitize the signal label for filenames
    sig_label_filename = sanitize_label_for_filename(process)
    
    # Plot
    for param in all_params:
        plt.figure()

        for label, data in zip([r'MinBias (13X)', process], [bkg_data, sig_data]):
            plt.hist(data[param], label=label, bins=50, density=True, log=True, histtype="step")
        
        plt.xlabel(f"{title} {param}")
        plt.legend()
        plt.title(title)
        plt.savefig(f"{sig_label_filename}_{title}_{param}.png")
        plt.show()
    