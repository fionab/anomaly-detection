import uproot
import numpy as np
from func import load_model, mse_loss
from sklearn.metrics import roc_curve, auc
from joblib import load
from plot import plot_input
import h5py
import matplotlib.pyplot as plt

def length_array(f, variable):
    """
    Parameters:
        f (tree): the tree of the data
        variable (str): name of the variable
    Returns:
        maximal length (i.e. number of events)
    """
    tree = f['l1PhaseIITree/L1PhaseIITree;1'][variable].arrays()
    lengths = [len(subtree[variable]) for subtree in tree]
    return max(lengths)


def create_met_array(f, var):
    
    pt = var + "Et"
    phi = var + "Phi"
    
    data_pt = f['l1PhaseIITree/L1PhaseIITree;1'][pt].arrays()
    data_phi = f['l1PhaseIITree/L1PhaseIITree;1'][phi].arrays()
    list_of_pt = data_pt[pt]
    list_of_phi = data_phi[phi]
    
    n = len(f['l1PhaseIITree/L1PhaseIITree;1'][pt].arrays())
    
    array = np.zeros((3, n))
    
    i = 0
    for event in range(0, n):
        array[0, event] = list_of_pt[event]
        array[2, event] = list_of_phi[event]
            
    return array


def data_formatting(f, particle, additional_params=[]):
    """
    Parameters:
        f (tree): tree of data
        particle (str): name of the particle in f
        additional_params (list of str): additional parameters
    Returns:
        array (arr): data with all the pt, eta, phi for the events, plus any additional parameters
    """
    particlePt = particle + "Pt"
    particleEta = particle + "Eta"
    particlePhi = particle + "Phi"
    
    maximal_number = length_array(f, particlePt)
    data_pt = f['l1PhaseIITree/L1PhaseIITree;1'][particlePt].arrays()[particlePt]
    data_eta = f['l1PhaseIITree/L1PhaseIITree;1'][particleEta].arrays()[particleEta]
    data_phi = f['l1PhaseIITree/L1PhaseIITree;1'][particlePhi].arrays()[particlePhi]

    additional_data = {param: f['l1PhaseIITree/L1PhaseIITree;1'][particle + param].arrays()[particle + param] for param in additional_params}
    
    number_of_events = len(data_pt)
    
    num_parameters = 3 + len(additional_params)  # 3 for pt, eta, phi plus additional parameters
    array = np.zeros((num_parameters * maximal_number, number_of_events))
    
    for i in range(number_of_events):
        for j in range(min(maximal_number, len(data_pt[i]))):
            base_index = j * num_parameters
            array[base_index, i] = data_pt[i][j] if j < len(data_pt[i]) else 0  # Fill data_pt
            array[base_index + 1, i] = data_eta[i][j] if j < len(data_eta[i]) else 0  # Fill data_eta
            array[base_index + 2, i] = data_phi[i][j] if j < len(data_phi[i]) else 0  # Fill data_phi
            for k, param in enumerate(additional_params):
                array[base_index + 3 + k, i] = additional_data[param][i][j] if j < len(additional_data[param][i]) else 0  # Fill additional parameters

    return array


def bring_sig_in_bkg_form(bkg, sig):
    '''
    Parameters:
        bkg(array): array with the background events for one particle
        sig(array): array with the signal events for one particle 
        
    Returns:
        Array: holds the extended sig array with the same number of rows as bkg
    '''
    #bkg = np.loadtxt(bkg, delimiter = ',')
    len_bkg = len(bkg)
    len_sig = len(sig)
    
    diff = np.abs(len_bkg - len_sig)
    n_events = len(np.transpose(sig))
    
    if diff != 0:
        add_zeros = np.zeros((diff, n_events))
        extended_array = np.vstack((sig, add_zeros))
        
        return(extended_array)
    
    else:
        return(sig)
    

def signal_formatting(signal, background, process, args=[]):
    """
    Parameters: 
        signal (str): the name of the signal file
        background (list of str): the background data for the different particles 
        args (list of str): name of additional parameters 
        process (str): signal process (might contain latex)
    Returns:
        signal_data (arr): array of the input data in the correct form for the algorithms
    """
    f = uproot.open(signal)
    
    particle_arrays = []
    met_array = create_met_array(f, "puppiMET")
    met_bkg = np.loadtxt(background[0], delimiter =',')
    plot_input(met_bkg, met_array, "puppiMET", process)
    particle_arrays.append(met_array)
    for i, particle in enumerate(["tkElectron", "tkPhoton", "gmtTkMuon", "nnTau", "seededConePuppiJet"]):
        part_bkg = np.loadtxt(background[i+1], delimiter=',')
        particle_data_stacked = data_formatting(f, particle, args)
        plot_input(part_bkg, particle_data_stacked, particle, process, additional_params=args)
        extended_data = bring_sig_in_bkg_form(part_bkg, particle_data_stacked)
        particle_arrays.append(extended_data)
                               
    signal_data = np.concatenate(particle_arrays, axis = 0)
                               
    return signal_data


def dae_prediction(signal_data, label, bkg, model):
    """
    Parameters:
        signal_data (arr): signal data
        label (str): name of the signal process
        bkg (str): name of bkg data
        model (str): name of the model
    Returns:
        fpr (arr): false positive rate
        tpr (arr): true positive rate
        auc (int): are under curve
    """
    autoencoder = load_model(model)
    #load data
    
    signal_data = np.transpose(signal_data)
        
    signal_labels = [label]
    
    signal_results = []
    signal_prediction = autoencoder.predict(signal_data)
    signal_results.append([signal_labels, signal_data, signal_prediction])
    #print("Shape of signal_results: ", signal_results)
    #print(np.shape(signal_results))
    
    #open bkg data.  ->if error: open also old sig (which do not apply to this signal here!!!)
    with h5py.File(bkg, 'r') as file:
        X_test = np.array(file['BKG_input'])
        bkg_prediction = np.array(file['BKG_predicted'])
    
    # compute loss value (true, predicted)
    total_loss = []
    total_loss.append(mse_loss(X_test, bkg_prediction.astype(np.float32)).numpy())
    #for i, signal_X in enumerate(signal_data):
        #total_loss.append(mse_loss(signal_X, signal_results[i][2].astype(np.float32)).numpy())
    total_loss.append(mse_loss(signal_data, signal_results[0][2].astype(np.float32)).numpy())
    
    """
    #plot the loss
    bin_size=100

    plt.figure(figsize=(10,8))
    for i, label in enumerate(signal_labels):
        plt.hist(total_loss[i], bins=bin_size, label=label, density = True, histtype='step', fill=False, linewidth=1.5)
    plt.yscale('log')
    plt.xlabel("Autoencoder Loss")
    plt.ylabel("Probability (a.u.)")
    plt.title('MSE loss')
    plt.legend(loc='best')
    #plt.savefig("First training_16_8_nodes")
    plt.show()
    
    bin_size=100
    """

    #fpr, tpr, auc
    target_background = np.zeros(total_loss[0].shape[0])
    labels = np.concatenate([['Background'], np.array(signal_labels)])
    for i, label in enumerate(labels):
        if i == 0: continue # background events
    
        trueVal = np.concatenate((np.ones(total_loss[i].shape[0]), target_background)) # anomaly=1, bkg=0
        predVal_loss = np.concatenate((total_loss[i], total_loss[0]))

        fpr_loss, tpr_loss, threshold_loss = roc_curve(trueVal, predVal_loss)

        auc_loss = auc(fpr_loss, tpr_loss)
        
    return fpr_loss, tpr_loss, auc_loss


def iso_prediction(signal, label, bkg, model):
    """
    Parameters:
        signal (arr): signal data
        bkg (str): name of the bkg data file
        label (str): name of the signal process
        model (str): name of the model
    Returns:
        fpr: false positive rate
        tpr: true positive rate
        auc: area under the curve
    """
    model = load(model)
    
    signal = np.transpose(signal)
    
    #open bkg data if error: open also old sig (which do not apply to this signal here!!!)
    with h5py.File(bkg, 'r') as file:
        X_test = np.array(file['BKG_input'])
    
    #signal prediction
    score_xtest = model.predict(X_test, output='score')
    score_signal = model.predict(signal, output='score')
    
    """
    #plot the score
    plt.figure()
    for score, label in zip([score_xtest, score_signal], ["X_test", label]):
        plt.hist(score, bins=50, label=label, density = True, histtype='step', fill=False, linewidth=1.5)
    plt.yscale('log')
    plt.xlabel("Score")
    plt.ylabel("Probability (a.u.)")
    plt.legend(loc='best')
    #plt.savefig("loss")
    plt.show()
    """
    
    #fpr, tpr, auc
    # Create labels for the test and signal data

    y_test = np.zeros(X_test.shape[0])
    y_signal = np.ones(signal.shape[0])

    # Combine the scores and the labels
    scores = np.concatenate([score_xtest, score_signal])
    y_true = np.concatenate([y_test, y_signal])

    # Calculate the ROC curve
    fpr, tpr, thresholds = roc_curve(y_true, scores)

    # Calculate the AUC
    roc_auc = auc(fpr, tpr)
    
    return fpr, tpr, roc_auc


def signal_prediction(signal, label, bkg, model, name):
    """
    Parameters: 
        signal (arr): signal data
        label (str): name of signal process
        bkg (str): name of bkg files used in dae
        model (str): name of the model file
        name (str): name of the algorithm
    Returns:
        fpr (arr): false positive rate
        tpr (arr): true positive rate
        auc (int): area under the ROC curve
    """
    if name == "DAE":
        fpr, tpr, auc = dae_prediction(signal, label, bkg, model)
     
    elif name == "Iso":
        fpr, tpr, auc = iso_prediction(signal, label, bkg, model)
        
    return fpr, tpr, auc