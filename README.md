# Anomaly Detection at Highluminosity LHC

In this repository are the notebooks I used for my semester project which delt with anomaly detection at the CMS detector at HL-LHC. The codes for Dense_AE.ipynb and func.py were taken from https://github.com/mpp-hep/ADC2021-examplecode and adapted to fit the samples I used.